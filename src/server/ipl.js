const fileSystem = require('fs');
const csvToJson = require('csvtojson');
const { Console } = require('console');

function getFile(path) {
    return csvToJson().fromFile(path);
}
function getMatches(path = '../data/matches.csv') {
    return getFile(path);
}
function getdeliveries(path = '../data/deliveries.csv') {
    return getFile(path);
}
function writeIntoJSONFile(path, array) {
    fileSystem.writeFile(path, JSON.stringify(array), err => {
        if (err) {
            console.error(err);
        } else {
            console.log('Successfully completed');
        }
    });
}

function matchesPlayedPerYear() {
    getMatches()
        .then((jsonArray) => {
            const numberOfMatchesPlayedPerYear = jsonArray
                .reduce((matchesPerYear, detailOfEachMatches) => {
                    if (matchesPerYear[detailOfEachMatches.season]) {
                        matchesPerYear[detailOfEachMatches.season]++;
                    }
                    else {
                        matchesPerYear[detailOfEachMatches.season] = 1;
                    }
                    return matchesPerYear;
                }, {});

            writeIntoJSONFile('../public/output/matchesPerYear.json', numberOfMatchesPlayedPerYear);
        });
}


function matchesWonPerTeam(year) {
    getMatches()
        .then((jsonArray) => {
            const numberOfMatchesWonPerTeam = jsonArray
                .reduce((matchesPerTeam, detailOfEachTeam) => {
                    if (matchesPerTeam[detailOfEachTeam.winner] && detailOfEachTeam.result === 'normal' && detailOfEachTeam.season === year) {
                        matchesPerTeam[detailOfEachTeam.winner]++;
                    }
                    else if (detailOfEachTeam.result === 'normal' && detailOfEachTeam.season === year) {
                        matchesPerTeam[detailOfEachTeam.winner] = 1;
                    }
                    return matchesPerTeam;
                }, {});
            writeIntoJSONFile('../public/output/matchesWonPerTeam.json', numberOfMatchesWonPerTeam);
        });
}

function extraRunsConcededPerTeam() {
    getMatches()
        .then((jsonArray) => {
            let idOf2016 = jsonArray.reduce((collectionOfId, currentObjectValue) => {
                if (currentObjectValue.season === '2016') {
                    collectionOfId[currentObjectValue.id] = '2016';
                }
                return collectionOfId;
            }, {});

            getdeliveries()
                .then((jsonArray) => {
                    let extraRunsOfEachTeam = jsonArray.reduce((totalExtraRuns, currentExtraRuns) => {
                        if (idOf2016[currentExtraRuns.match_id]) {
                            if (totalExtraRuns[currentExtraRuns.bowling_team]) {
                                totalExtraRuns[currentExtraRuns.bowling_team] =
                                    parseInt(totalExtraRuns[currentExtraRuns.bowling_team]) + parseInt(currentExtraRuns.extra_runs);
                            }
                            else {
                                totalExtraRuns[currentExtraRuns.bowling_team] = currentExtraRuns.extra_runs;
                            }
                        }
                        return totalExtraRuns;
                    }, {});
                    writeIntoJSONFile('../public/output/extraRunsPerTeam.json', extraRunsOfEachTeam);
                });
        });
}


function economicalBowlers() {
    getMatches()
        .then((jsonArray) => {
            let idOf2015 = jsonArray.reduce((collectionOfId, currentObjectValue) => {
                if (currentObjectValue.season === '2015') {
                    collectionOfId[currentObjectValue.id] = '2015';
                }
                return collectionOfId;
            }, {});

            getdeliveries()
                .then((jsonArray) => {
                    let allEconomicalBowlers = jsonArray.reduce((totalEconomicalBowlers, currentDetail) => {
                        if (idOf2015[currentDetail.match_id]) {
                            if (totalEconomicalBowlers[currentDetail.bowler]) {
                                totalEconomicalBowlers[currentDetail.bowler] =
                                    [totalEconomicalBowlers[currentDetail.bowler][0] + 1,
                                    totalEconomicalBowlers[currentDetail.bowler][1] + parseInt(currentDetail.total_runs)];
                            }
                            else {
                                totalEconomicalBowlers[currentDetail.bowler] =
                                    [1, parseInt(currentDetail.total_runs)];
                            }

                        }
                        return totalEconomicalBowlers;
                    }, {});
                    let top10Bowlers = [];
                    for (let key in allEconomicalBowlers) {
                        top10Bowlers.push([key, (allEconomicalBowlers[key][1] / (allEconomicalBowlers[key][0] / 6)).toFixed(2)]);
                    }
                    top10Bowlers.sort((a, b) => {
                        return a[1] - b[1];
                    });
                    for (let index = 10; index < top10Bowlers.length;) {
                        top10Bowlers.pop();
                    }
                    writeIntoJSONFile('../public/output/top10EconomicalBowlers.json', top10Bowlers);

                });

        });
}

function wonTossandMatch() {
    getMatches()
        .then(jsonArray => {
            let matchesWinnnerandTossWinner = jsonArray.reduce((totalWin, currentTeams) => {
                if (currentTeams.toss_winner === currentTeams.winner) {
                    if (totalWin[currentTeams.toss_winner]) {
                        totalWin[currentTeams.toss_winner]++;
                    } else {
                        totalWin[currentTeams.toss_winner] = 1;
                    }
                }
                return totalWin;
            }, {});
            writeIntoJSONFile('../public/output/matchesWonAndToss.json', matchesWinnnerandTossWinner);
        });
}

function manOfTheMatch() {
    getMatches()
        .then(jsonArray => {
            let maximumTimePlayerOfTheMatches = jsonArray.reduce((totalPlayerOfMatches, currentDetailOfTeam) => {
                if (totalPlayerOfMatches[currentDetailOfTeam.season]) {
                    if (totalPlayerOfMatches[currentDetailOfTeam.season][currentDetailOfTeam.player_of_match]) {
                        totalPlayerOfMatches[currentDetailOfTeam.season][currentDetailOfTeam.player_of_match]++;
                    } else {
                        totalPlayerOfMatches[currentDetailOfTeam.season][currentDetailOfTeam.player_of_match] = 1;
                    }
                }
                else {
                    totalPlayerOfMatches[currentDetailOfTeam.season] = { [currentDetailOfTeam.player_of_match]: 1 };
                }
                return totalPlayerOfMatches;
            }, {});
            let highestNumberOfPlayerOfTheMatch = {};
            Object.entries(maximumTimePlayerOfTheMatches).forEach((items) => {
                let highest = Object.entries(items[1]).reduce((acc, curr) => {
                    if (acc[1] < curr[1]) {
                        acc[0] = curr[0];
                        acc[1] = curr[1];
                    }
                    return acc;
                }, ['', 0]);
                highestNumberOfPlayerOfTheMatch[items[0] + ' - ' + highest[0]] = highest[1];
            });
            writeIntoJSONFile('../public/output/manOfTheMatch.json', highestNumberOfPlayerOfTheMatch);
        });
}

function strikeRateEachSeason(year) {
    getMatches()
        .then((jsonArray) => {
            let matchId = jsonArray.reduce((allId, currentDetail) => {
                if (currentDetail.season === year) {
                    allId[currentDetail.id] = year;
                }
                return allId;
            }, {});

            getdeliveries()
                .then((jsonArray) => {
                    let strikeRateOfplayer = jsonArray.reduce((playerAndStrikeRate, currentDetails) => {
                        if (matchId[currentDetails.match_id]) {
                            if (playerAndStrikeRate[currentDetails.batsman]) {
                                playerAndStrikeRate[currentDetails.batsman][0]++;
                                playerAndStrikeRate[currentDetails.batsman][1] += parseInt(currentDetails.batsman_runs);
                            }
                            else {
                                playerAndStrikeRate[currentDetails.batsman] = [1, parseInt(currentDetails.batsman_runs)];
                            }
                        }
                        return playerAndStrikeRate;
                    }, {});

                    Object.entries(strikeRateOfplayer).forEach((items) => {
                        strikeRateOfplayer[items[0]] = ((items[1][1] / items[1][0]) * 100).toFixed(2);
                    });
                    writeIntoJSONFile('../public/output/strikeRateOfPlayer.json', strikeRateOfplayer);
                });
        });
}
function getHighestWicket(playerWithHighestDismissed) {
    let highestWicket = Object.entries(playerWithHighestDismissed).reduce((maximumWicket, batsmanDetail) => {
        let highestWicketPerPlayer = Object.entries(batsmanDetail[1]).reduce((maximumWicketEachPlayer, batsmanDetail) => {
            if (batsmanDetail[1] > maximumWicketEachPlayer[1]) {
                [...maximumWicketEachPlayer] = batsmanDetail;
            }
            return maximumWicketEachPlayer;
        }, [0, 0]);
        if (maximumWicket[1][1] < highestWicketPerPlayer[1]) {
            maximumWicket = [batsmanDetail[0], highestWicketPerPlayer];
        }
        return maximumWicket;
    }, ['', ['', 0]]);
    return highestWicket;
}

function highestDismissed() {
    getdeliveries()
        .then((jsonArray) => {
            let playerWithHighestDismissed = jsonArray.reduce((totalDismissed, playerDetail) => {
                if (playerDetail.player_dismissed) {
                    if (totalDismissed[playerDetail.player_dismissed]) {
                        if (totalDismissed[playerDetail.player_dismissed][playerDetail.bowler]) {
                            totalDismissed[playerDetail.player_dismissed][playerDetail.bowler]++;
                        }
                        else {
                            totalDismissed[playerDetail.player_dismissed][playerDetail.bowler] = 1;
                        }
                    }
                    else {
                        totalDismissed[playerDetail.player_dismissed] = { [playerDetail.bowler]: 1 };
                    }
                }
                return totalDismissed;
            }, {});
            writeIntoJSONFile('../public/output/highestDismissedBySamePlayer.json', getHighestWicket(playerWithHighestDismissed));
        });
}

function bestEconomyInSuperOver() {
    getdeliveries()
        .then((jsonArray) => {
            let economyOfAllBowler = jsonArray.reduce((economyOfBowler, currentDetailOfBowler) => {
                if (currentDetailOfBowler.is_super_over != '0') {
                    if (economyOfBowler[currentDetailOfBowler.bowler]) {
                        economyOfBowler[currentDetailOfBowler.bowler] = [economyOfBowler[currentDetailOfBowler.bowler][0] + 1,
                        economyOfBowler[currentDetailOfBowler.bowler][1] + parseInt(currentDetailOfBowler.total_runs)];
                    }
                    else {
                        economyOfBowler[currentDetailOfBowler.bowler] = [1, parseInt(currentDetailOfBowler.total_runs)];
                    }
                }
                return economyOfBowler;
            }, {});
            let bestEconomyBowlerInSuperOver = Object.entries(economyOfAllBowler).reduce((bestBowler, bowlerDetail) => {
                let economyRate = (parseFloat(bowlerDetail[1][1]) / (parseFloat(bowlerDetail[1][0]) / 6.0));
                if (economyRate < bestBowler[1]) {
                    [...bestBowler] = [bowlerDetail[0], economyRate];
                }
                return bestBowler;
            }, ['', Number.MAX_VALUE]);

            writeIntoJSONFile('../public/output/bestEconomyBowlerInSuperOver.json', bestEconomyBowlerInSuperOver);
        });
}

matchesPlayedPerYear();
matchesWonPerTeam('2008');
extraRunsConcededPerTeam();
economicalBowlers();
wonTossandMatch();
manOfTheMatch();
strikeRateEachSeason('2015');
highestDismissed();
bestEconomyInSuperOver();

module.exports = {
    economicalBowlers,
    matchesPlayedPerYear,
    matchesWonPerTeam,
    extraRunsConcededPerTeam,
    wonTossandMatch,
    manOfTheMatch,
    strikeRateEachSeason,
    highestDismissed,
    bestEconomyInSuperOver
};